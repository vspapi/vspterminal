# README #

This README provides a summary of the services that are supported by the VSPTerminal web application.

### Invoice API ###
| Method | URL                             | Description       
|:-------|:--------------------------------|:-----------------------------------
| GET    | /api/invoice                    | Returns list of all invoices
| POST   | /api/invoice                    | Creates a new invoice 
| GET    | /api/invoice/:id                | Returns a specific invoice
| DELETE | /api/invoice/:id                | Deletes an invoice
| PUT    | /api/invoice/:id                | Updates an invoice
| POST   | /api/invoice/:id/item           | Adds a new item to an invoice 
| DELETE | /api/invoice/:id/item/:itm_id   | Deletes an item from an invoice
| PUT    | /api/invoice/:id/item/:itm_id   | Updates an item in an invoice

### Card API ###
| Method | URL                             | Description       
|:-------|:--------------------------------|:-----------------------------------
| GET    | /api/card                       | Return list of all cards 
| POST   | /api/card                       | Adds a new card to a customer 
| DELETE | /api/card/:card_id              | Deletes a card from a customer
| PUT    | /api/card/:card_id              | Updates a card for a customer
| GET    | /api/card/:card_id/payments     | Return list of card payments 
| GET    | /api/card/:card_id/refunds      | Return list of card refunds 

### Payment API ###
| Method | URL                             | Description       
|:-------|:--------------------------------|:-----------------------------------
| GET    | /api/payment                    | Return list of all payments 
| POST   | /api/payment                    | Initiates a payment for an invoice 
| PUT    | /api/payment/:refnum            | Voids a payment

### Refund API ###
| Method | URL                             | Description       
|:-------|:--------------------------------|:-----------------------------------
| GET    | /api/refund                     | Return list of all refunds 
| POST   | /api/refund                     | Initiates a refund 
| PUT    | /api/refund/:refnum             | Voids a refund