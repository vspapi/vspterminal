var mongoose = require('mongoose'); 
var vspapi = require('vspapi');
var sugar = require('sugar');
var app = require('./server/app.js');

var dburl = process.env.DB_URL || 'mongodb://localhost:27017/vspterm';

// Configure Database
console.log("Connecting to " + dburl);
mongoose.connect(dburl);  


vspapi.config({
      gid:  process.env.VSP_GID,
    appId:  process.env.VSP_APPID,
   userId:  process.env.VSP_UID
});

app.start();