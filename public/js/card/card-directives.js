(function(){
    
    console.log("Loading Card Directives ...");
    
    var app = angular.module("vspterm", []);
    
    app.directive("cardForm", function(){
        return {
            restrict: "E",
            templateUrl: "/partials/card-form.html",
            controllerAs: "formCtlr",
            controller: function() {
                this.card = {}
                
                this.addCard = function() {
                  console.log("Add Card: " + this.card.number);
                  this.card = {};  
                };
            }
        };
    });
    
})();