var expect = require('expect.js');
var mongoose = require('mongoose'); 
var Invoice = require("../server/models/invoiceModel.js");
var Sample = require('./sampleInvoices.js');

var dburl = process.env.DB_URL || 'mongodb://localhost:27017/vspterm';

// Configure Database
console.log("Connecting to " + dburl);
mongoose.connect(dburl);  



describe('Invoice API', function(){
    it('Has correctly computed values', function(done){
        Invoice.create(Sample.invoice, function(err, invoice){
            
            
            //console.log(invoice);
            expect(err).to.be(null);
            expect(invoice.number).to.eql(Sample.invoice.number);
            expect(invoice.tax).to.eql(710);
            
            expect(invoice.subtotal).to.eql(10138);
            
            expect(invoice.total).to.eql(10631.04);
            
            done();
        });
    });
    
});