var request = require('superagent');
var expect = require('expect.js');
var cards = require('./sampleCards.js');

var baseUrl = 'http://' + process.env.IP + ':' + process.env.PORT;


describe('Card API', function(){
    this.timeout(150000);
    
    it('Can tokenize a card, lookup the card, update the card info, and then delete it.', function(done){
        
        // 1. Create a New Card
        request.post(baseUrl + '/api/card')
        .send(cards.amex)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .end(function(err, res) {
           expect(err).to.be(null);
           expect(res.body.status).to.eql('1');
           expect(res.body.data.token).to.not.eql(null);
           expect(res.body.data.type).to.eql('AMEX');
           
           // 2. Lookup a card
           var _id = res.body.data._id; 
           request.get(baseUrl + '/api/card/' + _id)
           .set('Accept', 'application/json')
           .set('Content-Type', 'application/json')
           .end(function(err, res){
               expect(err).to.be(null);
               expect(res.body.status).to.eql('1');
               expect(res.body.data).to.not.eql(null);
               
               var card = res.body.data;
               card.expiration.month = "10";
               card.expiration.year = "17";
               card.cvv = "5555";
               
               // 3. update card
               request.put(baseUrl + '/api/card/' + _id)
               .set('Accept', 'application/json')
               .set('Content-Type', 'application/json')
               .send(card)
               .end(function(err, res){
                   expect(err).to.be(null);
                   expect(res.body.status).to.eql('1');
                   expect(res.body.data).to.not.eql(null);
                   expect(res.body.data.expiration).to.eql(card.expiration);
                   
                   // 4. delete card
                   request.delete(baseUrl + '/api/card/' + _id)
                   .set('Accept', 'application/json')
                   .set('Content-Type', 'application/json')
                   .end(function(err, res){
                       expect(err).to.be(null);
                       expect(res.body.status).to.eql('1');
                       expect(res.body.data).to.not.eql(null);
                       
                       done();
                   });
                   
               });
            });
        });
        
        
    });
    
    it('Should return a list of cards', function(done){
        request.get(baseUrl + '/api/card')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .end(function(err, res) {
            expect(err).to.be(null);
            expect(res.body.status).to.eql('1');
            done();
        });
    });
});
