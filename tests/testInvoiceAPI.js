var request = require('superagent');
var expect = require('expect.js');
var Sample = require('./sampleInvoices.js');


var baseUrl = 'http://' + process.env.IP + ':' + process.env.PORT;


describe('Invoice API', function(){
    this.timeout(150000);
    
    it('Can Get Invoices', function(done){
        request.get(baseUrl + '/api/invoice')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .end(function(err, res) {
            expect(err).to.be(null);
            expect(res.body.status).to.eql('1');
            
            done();
        });
    });
    
    it('Can Create Invoice', function(done){
        var invoice = JSON.parse(JSON.stringify( Sample.invoice ));
        invoice.items = Sample.items;
        
        createInvoice(invoice, function(err, res) {
            var invoice = res.body.data;
            
            expect(err).to.be(null);
            expect(res.body.status).to.eql('1');
            expect(invoice.number).to.eql(Sample.invoice.number);
            expect(invoice.tax).to.eql(710);
            expect(invoice.subtotal).to.eql(10138);
            expect(invoice.total).to.eql(10631.04);
            
            deleteInvoice(invoice._id, function(err, res) {
                expect(err).to.be(null);
                expect(res.body.status).to.eql('1');
                done();
            });
            
        });
    });
    
    it('Can Add Item', function(done){
        var invoice = Sample.invoice;
        var items = Sample.items;
        
        createInvoice(invoice, function(err, res) {
            expect(err).to.be(null);
            expect(res.body.status).to.eql('1');
            
            invoice = res.body.data;
            expect(invoice.total).to.eql(0);
            
            addItem(invoice._id, items[0], function(err, res){
                expect(err).to.be(null);
                expect(res.body.status).to.eql('1');
                
                invoice = res.body.data;
                expect(invoice.items.length).to.eql(1);
                expect(invoice.total).to.eql(10495.8);
                
                done();
            });
            
        });
    });
 
});

function addItem(_id, item, handle) {
    request.post(baseUrl + '/api/invoice/' + _id + '/item')
    .send(item)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .end(handle);
}

function deleteInvoice(_id, handle) {
    request.delete(baseUrl + '/api/invoice/' + _id)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .end(handle);
}

function createInvoice(invoice, handle) {
    request.post(baseUrl + '/api/invoice')
    .send(invoice)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .end(handle);
}