exports.invoice = {
    number: "00001",
    customer: "5692fcf10dd5207ce891eb58",
    memo: "Jan 12th Services Invoice",
    message: "Thank you for your business.",
    taxRate: 0.071,
    discount: {
        value: 2
    }
}

exports.items = [{
    product: "SkyClub SR22T Membership",
    description: "25Hr Membership for the SR22T",
    quantity: 25,
    rate: 400,
    taxed: true,
    category: "Aircraft Rental"
},{
    product: "Flight Instruction",
    description: "Flight Instruction (SkyClub Member)",
    quantity: 2.3,
    rate: 60,
    taxed: false,
    category: "Aircraft Rental"
}]