var controller = require('../controllers/refundController.js');

exports.setup = function(app) {
    app.get('/api/refund', controller.getRefunds);
    
    app.post('/api/refund', controller.processRefund);
    
    app.delete('/api/refund/:refnum', controller.voidRefund);
}