var controller = require('../controllers/cardController.js');

exports.setup = function(app) {
    
    app.get('/api/card/:card_id', controller.getCard);
    
    app.get('/api/card', controller.getCards);
    
    app.post('/api/card', controller.createCard);
    
    app.delete('/api/card/:card_id', controller.deleteCard);
    
    app.put('/api/card/:card_id', controller.updateCard);
}