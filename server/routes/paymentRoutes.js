var controller = require('../controllers/paymentController.js');

exports.setup = function(app) {
    app.get('/api/payment', controller.getPayments);
    
    app.post('/api/payment', controller.processPayment);
    
    app.delete('/api/payment/:refnum', controller.voidPayment);
}