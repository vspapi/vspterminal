var controller = require('../controllers/invoiceController.js');

exports.setup = function(app) {
    app.get('/api/invoice/:invoice_id', controller.getInvoice);
    
    app.get('/api/invoice', controller.getAllInvoices);
    
    app.post('/api/invoice', controller.createNewInvoice);
    
    app.delete('/api/invoice/:invoice_id', controller.deleteInvoice);
    
    app.put('/api/invoice/:invoice_id', controller.updateInvoice);
    
    app.post('/api/invoice/:invoice_id/item', controller.addInvoiceItem);
    
    app.delete('/api/invoice/:invoice_id/item/:item_id', controller.deleteInvoiceItem);
    
    app.put('/api/invoice/:invoice_id/item/:item_id', controller.updateInvoiceItem);
}