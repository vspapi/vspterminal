var Model = require("../models/invoiceModel.js");
var Invoice = Model.Invoice;
var VSPAPI = require('vspapi');
var util = require('./utils.js');

exports.getAllInvoices = function(req, res) {
    Invoice.find(function(err, invoices) {
        util.handle(err, invoices, res);
    });
}

exports.createNewInvoice = function(req, res) {
    Invoice.create(req.body, function(err, invoice){
        util.handle(err, invoice, res);
    });
}

exports.getInvoice = function(req, res) {
    Invoice.findById(req.params.invoice_id, function(err, invoice) {
        util.handle(err, invoice, res);
    });
}

exports.deleteInvoice = function(req, res) {
    Invoice.findById(req.params.invoice_id, function(err, invoice) {
        if (err) {
            util.handleErr(err, res);
        } else {
            if(!!!invoice.payment) {
                invoice.remove();
                exports.getAllInvoices(req, res);
            } else {
                util.error("Cannot delete invoice once payment has been processed.", res);
            }
        }
    }); 
}

exports.updateInvoice = function(req, res) {
    Invoice.findOneAndUpdate({_id:req.params.invoice_id}, req.body, {'new':true}, function(err, invoice){
        util.handle(err, invoice, res);
    });
}

exports.addInvoiceItem = function(req, res) {
    Invoice.findById(req.params.invoice_id, function(err, invoice){
        if (err) {
            util.handleErr(err, res);
        } else {
            if(!!!invoice.payment) {
                if(!!invoice.items) {
                    invoice.items = new Array();
                }
                
                invoice.items.push(req.body);
                invoice.save(function(err, invoice){
                    util.handle(err, invoice, res);
                });
            } else {
                util.error("Cannot alter an invoice once payment has been processed.", res);
            }
        }
    });
}

exports.deleteInvoiceItem = function(req, res) {
    Invoice.findById(req.params.invoice_id, function(err, invoice){
        if (err) {
            util.handleErr(err, res);
        } else {
            if(!!!req.body.payment) {
                invoice.items.remove(function(item){
                    item == req.params.item_id;
                })
                invoice.save();
                util.handleOk(invoice, res);
            } else {
                util.error("Cannot alter an invoice once payment has been processed.", res);
            }
        }
    });
}

exports.updateInvoiceItem = function(req, res) {
    Invoice.findById(req.params.invoice_id, function(err, invoice) {
        if (err) {
            util.handleErr(err, res);
        } else {
            if(!!req.body.payment) {
                
            } else {
                util.error("Cannot alter an invoice once payment has been processed.", res);
            }
        }
    });
}