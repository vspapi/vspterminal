exports.handleOk = function(data, res) {
    res.json({
        'status' : 1,
        'message' : 'ok',
        'data' : data
    });
}

exports.handleErr = function(err, res) {
    res.json({
        'status' : 0,
        'message' : err.message,
        'err' : err
    });
}

exports.handleReply = function(reply, res) {
    if (reply.status == 1) {
       res.json({
            'status' : 0,
            'message' : reply.resultMessage
        });
    }  
}

exports.error = function(msg, res) {
    res.json({
        'status': 0,
        'message': msg
    })
}

exports.handle = function(err, data, res) {
    if (err) {
        exports.handleErr(err, res)
   } else {
       exports.handleOk(data, res)
   }
}