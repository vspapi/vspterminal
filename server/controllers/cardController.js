var Card = require("../models/cardModel.js");
var VSPAPI = require('vspapi');
var util = require('./utils.js');

/**
 * Return list of all cards for a customer 
 */
exports.getCards = function(req, res) {
    // use mongoose to get all cards in the database
    Card.find(function(err, cards) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        util.handleErr(err, res);
        util.handleOk(cards, res);
    });
}

/**
 * Return card with specified id
 */
exports.getCard = function(req, res) {
    Card.findById(req.params.card_id, function(err, card) {
        util.handleErr(err, res);
        util.handleOk(card, res);
    });
}

/**
 * Tokenizes and then stores a card's data. The stored record is returned.
 */
exports.createCard = function(req, res) {
    var card = req.body;
    VSPAPI.createToken({
        card: card
    }, function(reply) {
        util.handleReply(reply, res); 
        card.type = _getCardType(card.number);
        delete card.number;
        delete card.cvv;
        card.token = reply.token;
        Card.create(card, function(err, newCard){
            util.handleErr(err, res);
            util.handleOk(newCard, res);
        })
    });
}

/**
 * Deletes a tokenized card from the database
 */
exports.deleteCard = function(req, res) {
    // extract token from request body
    Card.findOneAndRemove({_id:req.params.card_id}, function(err, card, result) {
        util.handleErr(err, res);
        
        // attempt to delete token from processor
        VSPAPI.deleteToken(card.toObject().token, function(reply) {
           util.handleReply(reply, res);
           exports.getCards(req, res);
        });
    });
}

/**
 * Updates a tokenzied card in the database
 */
exports.updateCard = function(req, res) {
    var update = req.body;
    Card.findOneAndUpdate({_id: req.params.card_id}, update, {'new':true}, function(err, card){
       util.handleErr(err, res);
       
       VSPAPI.updateToken(card, function(reply){
            util.handleReply(reply, res);
            util.handleOk(card, res);
       });
    });
}

function _getCardType(cnum) {
    var fchar = Number.parseInt(cnum.charAt(0));
    
    switch (fchar) {
        case 3 : // Amex or Dinners Club or Carte Blanche
            var amexPattern = /^3(4|7)\d{13}$/g;
            var dinnersPattern = /^3(0|6|8)\d{12}$/g;
            return (amexPattern.test(cnum)) ? "AMEX" : (dinnersPattern.test(cnum))? "DC" : "Unsupported";
        case 6 : // Discover
            var first8Digits = cnum.substring(0, 8);
            return (cnum.length == 16 && 
                (_inRange(first8Digits, "60110000", "60119999") ||
                _inRange(first8Digits, "65000000", "65999999") ||
                _inRange(first8Digits, "62212600", "62292599"))) ? "DSCV" : "Unsupported";
        case 5 : // Master Card
            var mcPattern = /^5[1-5]\d{14}$/g;
            return (mcPattern.test(cnum))? "MC" : "Unsupported";
        case 4 : // Visa
            return (cnum.length == 13 || cnum.length == 16)? "VISA" : "Unsupported";
        
    }
}

function _inRange(value, min, max) {
    value = Number.parseInt(value);
    min = Number.parseInt(min);
    max = Number.parseInt(max);
    
    return (value >= min && value <= max);
}