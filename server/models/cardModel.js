var mongoose = require('mongoose');
 
var CardSchema = mongoose.Schema({
    token: String,
    type: String,
    cardholder: {
    	firstName: String,
    	lastName: String
    },
    expiration: {
    	month: String,
    	year: String
    },
    avs: {
    	street: String,
    	zip: String
    },
});


module.exports = mongoose.model('Card', CardSchema);

