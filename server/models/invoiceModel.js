var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var ItemSchema = new Schema();

ItemSchema.add({
   product: String,
   description: String,
   quantity: {type: Number, default: 1},
   rate: Number,
   taxed: {type: Boolean, default: false},
   amount: {type: Number, default: 0},
   category: String
});

ItemSchema.set('toJSON', { getters: true, virtuals: true });

var InvoiceSchema = Schema({
    number: String,
    customer: ObjectId,
    invoiceDate: {type: Date, default: Date.now},
    dueDate: {type: Date, default: Date.now},
    memo: String,
    message: String,
    taxRate: {type:Number, default:0},
    discount: {
        type: {type:String, default:'percent'}, 
        value: {type:Number, default:0}
    },
    items: [ItemSchema],
    tax: {type:Number, default:0},
    subtotal: {type:Number, default:0},
    total: {type:Number, default:0},
    payment: {type: ObjectId, ref: 'Payment'}
});

InvoiceSchema.set('toJSON', { getters: true, virtuals: true });

InvoiceSchema.pre('save', function(next){
    console.log("Recomputing invoice " + this._id);
    this.tax = 0;
    this.subtotal = 0;
    for (var ndx=0; ndx<this.items.length; ndx++) {
        var item = this.items[ndx];
        item.amount = item.rate * item.quantity;
        this.tax += (item.taxed)? item.amount * this.taxRate : 0;
        this.subtotal += item.amount;
    }
    this.total = this.subtotal + this.tax;
    if (this.discount.type === 'percent') {
        this.total = this.total - (this.total * (this.discount.value/100));
    }
    
    if (this.discount.type === 'value') {
        this.total = this.total - this.discount.value;
    }
    
    this.subtotal = this.subtotal.toFixed(2);
    this.total = this.total.toFixed(2);
    this.tax = this.tax.toFixed(2);
    next();
})

exports.Invoice = mongoose.model('Invoice', InvoiceSchema);