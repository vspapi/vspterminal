var express = require('express');
var app = express(); // create our app w/ express
var morgan = require('morgan'); // log requests to the console (express4)
var bodyParser = require('body-parser'); // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

function start() {
    console.log("static: " + __dirname);
    // Configure server app
    app.use(express.static(__dirname + '/../public'));                 // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());
    
    // Setup all routes
    var routes = require('./routes');
    routes.cardRoutes.setup(app);
    routes.invoiceRoutes.setup(app);
    routes.paymentRoutes.setup(app);
    routes.refundRoutes.setup(app);

    app.listen(process.env.PORT);
    console.log("App listening on port " + process.env.PORT);
}

exports.start = start;
exports.app = app;